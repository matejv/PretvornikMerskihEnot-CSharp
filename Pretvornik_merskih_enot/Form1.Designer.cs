﻿namespace Pretvornik_merskih_enot
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBoxIzberi = new System.Windows.Forms.GroupBox();
            this.radioPovrsina = new System.Windows.Forms.RadioButton();
            this.radioMasa = new System.Windows.Forms.RadioButton();
            this.radioProstornina = new System.Windows.Forms.RadioButton();
            this.radioCas = new System.Windows.Forms.RadioButton();
            this.radioHitrost = new System.Windows.Forms.RadioButton();
            this.radioTemperatura = new System.Windows.Forms.RadioButton();
            this.radioDolzina = new System.Windows.Forms.RadioButton();
            this.groupBoxPretvorba = new System.Windows.Forms.GroupBox();
            this.textBoxDesno = new System.Windows.Forms.TextBox();
            this.textBoxLevo = new System.Windows.Forms.TextBox();
            this.buttonZamenjaj = new System.Windows.Forms.Button();
            this.comboDesno = new System.Windows.Forms.ComboBox();
            this.comboLevo = new System.Windows.Forms.ComboBox();
            this.buttonPonastavi = new System.Windows.Forms.Button();
            this.buttonPretvori = new System.Windows.Forms.Button();
            this.panelOzadje = new System.Windows.Forms.Panel();
            this.groupBoxIzberi.SuspendLayout();
            this.groupBoxPretvorba.SuspendLayout();
            this.panelOzadje.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxIzberi
            // 
            resources.ApplyResources(this.groupBoxIzberi, "groupBoxIzberi");
            this.groupBoxIzberi.Controls.Add(this.radioPovrsina);
            this.groupBoxIzberi.Controls.Add(this.radioMasa);
            this.groupBoxIzberi.Controls.Add(this.radioProstornina);
            this.groupBoxIzberi.Controls.Add(this.radioCas);
            this.groupBoxIzberi.Controls.Add(this.radioHitrost);
            this.groupBoxIzberi.Controls.Add(this.radioTemperatura);
            this.groupBoxIzberi.Controls.Add(this.radioDolzina);
            this.groupBoxIzberi.Name = "groupBoxIzberi";
            this.groupBoxIzberi.TabStop = false;
            // 
            // radioPovrsina
            // 
            resources.ApplyResources(this.radioPovrsina, "radioPovrsina");
            this.radioPovrsina.Name = "radioPovrsina";
            this.radioPovrsina.TabStop = true;
            this.radioPovrsina.UseVisualStyleBackColor = true;
            this.radioPovrsina.CheckedChanged += new System.EventHandler(this.radioPovrsina_CheckedChanged);
            // 
            // radioMasa
            // 
            resources.ApplyResources(this.radioMasa, "radioMasa");
            this.radioMasa.Name = "radioMasa";
            this.radioMasa.TabStop = true;
            this.radioMasa.UseVisualStyleBackColor = true;
            this.radioMasa.CheckedChanged += new System.EventHandler(this.radioMasa_CheckedChanged);
            // 
            // radioProstornina
            // 
            resources.ApplyResources(this.radioProstornina, "radioProstornina");
            this.radioProstornina.Name = "radioProstornina";
            this.radioProstornina.TabStop = true;
            this.radioProstornina.UseVisualStyleBackColor = true;
            this.radioProstornina.CheckedChanged += new System.EventHandler(this.radioProstornina_CheckedChanged);
            // 
            // radioCas
            // 
            resources.ApplyResources(this.radioCas, "radioCas");
            this.radioCas.Name = "radioCas";
            this.radioCas.TabStop = true;
            this.radioCas.UseVisualStyleBackColor = true;
            this.radioCas.CheckedChanged += new System.EventHandler(this.radioCas_CheckedChanged);
            // 
            // radioHitrost
            // 
            resources.ApplyResources(this.radioHitrost, "radioHitrost");
            this.radioHitrost.Name = "radioHitrost";
            this.radioHitrost.TabStop = true;
            this.radioHitrost.UseVisualStyleBackColor = true;
            this.radioHitrost.CheckedChanged += new System.EventHandler(this.radioHitrost_CheckedChanged);
            // 
            // radioTemperatura
            // 
            resources.ApplyResources(this.radioTemperatura, "radioTemperatura");
            this.radioTemperatura.Name = "radioTemperatura";
            this.radioTemperatura.TabStop = true;
            this.radioTemperatura.UseVisualStyleBackColor = true;
            this.radioTemperatura.CheckedChanged += new System.EventHandler(this.radioTemperatura_CheckedChanged);
            // 
            // radioDolzina
            // 
            resources.ApplyResources(this.radioDolzina, "radioDolzina");
            this.radioDolzina.Name = "radioDolzina";
            this.radioDolzina.TabStop = true;
            this.radioDolzina.UseVisualStyleBackColor = true;
            this.radioDolzina.CheckedChanged += new System.EventHandler(this.radioDolzina_CheckedChanged);
            // 
            // groupBoxPretvorba
            // 
            resources.ApplyResources(this.groupBoxPretvorba, "groupBoxPretvorba");
            this.groupBoxPretvorba.Controls.Add(this.textBoxDesno);
            this.groupBoxPretvorba.Controls.Add(this.textBoxLevo);
            this.groupBoxPretvorba.Controls.Add(this.buttonZamenjaj);
            this.groupBoxPretvorba.Controls.Add(this.comboDesno);
            this.groupBoxPretvorba.Controls.Add(this.comboLevo);
            this.groupBoxPretvorba.Name = "groupBoxPretvorba";
            this.groupBoxPretvorba.TabStop = false;
            // 
            // textBoxDesno
            // 
            resources.ApplyResources(this.textBoxDesno, "textBoxDesno");
            this.textBoxDesno.Name = "textBoxDesno";
            this.textBoxDesno.ReadOnly = true;
            // 
            // textBoxLevo
            // 
            resources.ApplyResources(this.textBoxLevo, "textBoxLevo");
            this.textBoxLevo.Name = "textBoxLevo";
            // 
            // buttonZamenjaj
            // 
            resources.ApplyResources(this.buttonZamenjaj, "buttonZamenjaj");
            this.buttonZamenjaj.Image = global::Pretvornik_merskih_enot.Properties.Resources.Zamenjaj;
            this.buttonZamenjaj.Name = "buttonZamenjaj";
            this.buttonZamenjaj.UseVisualStyleBackColor = true;
            this.buttonZamenjaj.Click += new System.EventHandler(this.buttonZamenjaj_Click);
            // 
            // comboDesno
            // 
            resources.ApplyResources(this.comboDesno, "comboDesno");
            this.comboDesno.FormattingEnabled = true;
            this.comboDesno.Name = "comboDesno";
            // 
            // comboLevo
            // 
            resources.ApplyResources(this.comboLevo, "comboLevo");
            this.comboLevo.FormattingEnabled = true;
            this.comboLevo.Name = "comboLevo";
            // 
            // buttonPonastavi
            // 
            resources.ApplyResources(this.buttonPonastavi, "buttonPonastavi");
            this.buttonPonastavi.Name = "buttonPonastavi";
            this.buttonPonastavi.UseVisualStyleBackColor = true;
            this.buttonPonastavi.Click += new System.EventHandler(this.buttonPonastavi_Click);
            // 
            // buttonPretvori
            // 
            resources.ApplyResources(this.buttonPretvori, "buttonPretvori");
            this.buttonPretvori.Name = "buttonPretvori";
            this.buttonPretvori.UseVisualStyleBackColor = true;
            this.buttonPretvori.Click += new System.EventHandler(this.buttonPretvori_Click);
            // 
            // panelOzadje
            // 
            resources.ApplyResources(this.panelOzadje, "panelOzadje");
            this.panelOzadje.BackColor = System.Drawing.SystemColors.HighlightText;
            this.panelOzadje.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelOzadje.Controls.Add(this.groupBoxIzberi);
            this.panelOzadje.Controls.Add(this.groupBoxPretvorba);
            this.panelOzadje.Name = "panelOzadje";
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelOzadje);
            this.Controls.Add(this.buttonPonastavi);
            this.Controls.Add(this.buttonPretvori);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.groupBoxIzberi.ResumeLayout(false);
            this.groupBoxIzberi.PerformLayout();
            this.groupBoxPretvorba.ResumeLayout(false);
            this.groupBoxPretvorba.PerformLayout();
            this.panelOzadje.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxIzberi;
        private System.Windows.Forms.RadioButton radioPovrsina;
        private System.Windows.Forms.RadioButton radioMasa;
        private System.Windows.Forms.RadioButton radioProstornina;
        private System.Windows.Forms.RadioButton radioCas;
        private System.Windows.Forms.RadioButton radioHitrost;
        private System.Windows.Forms.RadioButton radioTemperatura;
        private System.Windows.Forms.RadioButton radioDolzina;
        private System.Windows.Forms.GroupBox groupBoxPretvorba;
        private System.Windows.Forms.ComboBox comboDesno;
        private System.Windows.Forms.ComboBox comboLevo;
        private System.Windows.Forms.Button buttonZamenjaj;
        private System.Windows.Forms.Button buttonPretvori;
        private System.Windows.Forms.TextBox textBoxDesno;
        private System.Windows.Forms.TextBox textBoxLevo;
        private System.Windows.Forms.Button buttonPonastavi;
        private System.Windows.Forms.Panel panelOzadje;
    }
}

