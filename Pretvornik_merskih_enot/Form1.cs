﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pretvornik_merskih_enot
{
    public partial class Form1 : Form
    {
        double stevilo = 0.0;
        int idxLevo = 0;
        int idxDesno = 0;
        int radioIzbira = 0;

        public Form1()
        {
            InitializeComponent();
            radioDolzina.Checked = true;
        }

        private void nastaviCombo()
        {
            //v spodnjih tabelah so shranjeni nizi, ki se izpišejo v spustnem seznamu ob izbiri določene količine
            string[] dolzina = new string[7] { "Milimeter", "Centimeter", "Decimeter", "Meter", "Kilometer", "Palec", "Milja" };
            string[] masa = new string[5] { "Miligram", "Gram", "Dekagram", "Kilogram", "Tona" };
            string[] hitrost = new string[4] { "Kilometer na uro", "Meter na sekundo", "Milja na uro", "Vozel" };
            string[] cas = new string[5] { "Milisekunda", "Sekunda", "Minuta", "Ura", "Dan" };
            string[] temperatura = new string[3] { "Stopinja Celzija", "Kelvin", "Stopinja Fahrenheita" };
            string[] povrsina = new string[8] { "Kvadratni milimeter", "Kvadratni centimeter", "Kvadratni decimeter", "Kvadratni meter", "Kvadratni kilometer", "Hektar", "Kvadratni palec", "Kvadratna milja" };
            string[] prostornina = new string[8] { "Kubični milimeter", "Kubični centimeter", "Kubični decimeter", "Kubični meter", "Mililiter", "Deciliter", "Liter", "Kubični palec" };

            comboLevo.Items.Clear(); //počisti spustni seznam
            comboDesno.Items.Clear();

            switch (radioIzbira) //v spustni seznam na podlagi izbire količine doda ustrezne enote iz polja
            {
                case 0: comboLevo.Items.AddRange(dolzina); comboDesno.Items.AddRange(dolzina); break;
                case 1: comboLevo.Items.AddRange(masa); comboDesno.Items.AddRange(masa); break;
                case 2: comboLevo.Items.AddRange(hitrost); comboDesno.Items.AddRange(hitrost); break;
                case 3: comboLevo.Items.AddRange(cas); comboDesno.Items.AddRange(cas); break;
                case 4: comboLevo.Items.AddRange(temperatura); comboDesno.Items.AddRange(temperatura); break;
                case 5: comboLevo.Items.AddRange(povrsina); comboDesno.Items.AddRange(povrsina); break;
                case 6: comboLevo.Items.AddRange(prostornina); comboDesno.Items.AddRange(prostornina); break;
            }

            comboLevo.SelectedIndex = 0; //nastavi, da je že na začetku izbran prvi element seznama
            comboDesno.SelectedIndex = 0;

            textBoxLevo.Text = ""; //ponastavi še TextBoxe, da so prazni
            textBoxDesno.Text = "";
        }

        private bool preveriVnos() //če uporabnik ni vnesel št. za pretvorbo, metoda vrne false
        {
            if (textBoxLevo.Text.Length < 1)
            {
                textBoxDesno.Text = "";
                MessageBox.Show("Neveljaven vnos! Vnesi število za pretvorbo.");
                return false;
            }
            return true;
        }

        private void pretvorba()
        {
            if (preveriVnos() == true)
            {
                stevilo = Convert.ToDouble(textBoxLevo.Text);

                idxLevo = comboLevo.SelectedIndex;
                idxDesno = comboDesno.SelectedIndex;

                pretvarjanje novo = new pretvarjanje();
                textBoxDesno.Text = novo.pretvori(radioIzbira, idxLevo, idxDesno, stevilo).ToString();
            }
        }

        private void radioDolzina_CheckedChanged(object sender, EventArgs e)
        {
            radioIzbira = 0;

            nastaviCombo();
        }

        private void radioMasa_CheckedChanged(object sender, EventArgs e)
        {
            radioIzbira = 1;

            nastaviCombo();
        }

        private void radioHitrost_CheckedChanged(object sender, EventArgs e)
        {
            radioIzbira = 2;

            nastaviCombo();
        }

        private void radioCas_CheckedChanged(object sender, EventArgs e)
        {
            radioIzbira = 3;

            nastaviCombo();
        }

        private void radioTemperatura_CheckedChanged(object sender, EventArgs e)
        {
            radioIzbira = 4;

            nastaviCombo();
        }

        private void radioPovrsina_CheckedChanged(object sender, EventArgs e)
        {
            radioIzbira = 5;

            nastaviCombo();
        }

        private void radioProstornina_CheckedChanged(object sender, EventArgs e)
        {
            radioIzbira = 6;

            nastaviCombo();
        }

        private void buttonPretvori_Click(object sender, EventArgs e)
        {
            pretvorba();
        }

        private void buttonPonastavi_Click(object sender, EventArgs e)
        {
            stevilo = 0.0;
            textBoxLevo.Text = "";
            textBoxDesno.Text = "";
            comboLevo.SelectedIndex = 0;
            comboDesno.SelectedIndex = 0;
        }

        private void buttonZamenjaj_Click(object sender, EventArgs e)
        {
            pretvorba(); //preden obrne, še pretvori v primeru, da uporabnik še ni kliknil na gumb "Pretvori"

            //zamenja tudi izbran element v spustnih seznamih ...
            comboLevo.SelectedIndex = idxDesno;
            comboDesno.SelectedIndex = idxLevo;

            //... in ponovno shrani indekse izbranih elementov za primer ponovne menjave
            idxLevo = comboLevo.SelectedIndex;
            idxDesno = comboDesno.SelectedIndex;

            //zamenja tudi števila v TextBoxih
            string temp = textBoxLevo.Text;
            textBoxLevo.Text = textBoxDesno.Text;
            textBoxDesno.Text = temp;
        }
    }

    class pretvarjanje
    {
        private double koeficient = 0.0; //v to spremenljivko shranimo koeficient za množenje iz tabele

        //tabele vsebujejo koeficiente za množenje vnesenega števila določene količine, da dobimo to število v drugi merski enoti
        //grafično ponazorjen primer tabele: http://sdrv.ms/12BZVpf
        private double[,] dolzina = new double[7, 7] {
                { 1, 0.1, 0.01, 0.001, 0.000001, 0.03937, 0.00000006214 },
                { 10, 1, 0.1, 0.01, 0.00001, 0.3937, 0.0000006214 },
                { 100, 10, 1, 0.1, 0.0001, 3.937, 0.000006214 },
                { 1000, 100, 10, 1, 0.001, 39.37, 0.00006214 },
                { 1000000, 100000, 10000, 1000, 1, 39370.079, 0.6214 },
                { 25.4, 2.54, 0.254, 0.0254, 0.0000254, 1, 0.0000015782 },
                { 1609344, 160934.4, 16093.44, 1609.344, 1.609344, 63360, 1}};

        private double[,] masa = new double[5, 5] {
                { 1, 0.001, 0.0001, 0.00001, 0.000000001 },
                { 1000, 1, 0.1, 0.001, 0.000001 },
                { 10000, 10, 1, 0.01, 0.00001 },
                { 1000000, 1000, 100, 1, 0.001 },
                { 1000000000, 1000000, 100000, 1000, 1 }};

        private double[,] hitrost = new double[4, 4] {
                { 1, 0.2778, 0.6214, 0.54 },
                { 3.6, 1, 2.2369, 1.9438 },
                { 1.6093, 0.447, 1, 0.869 },
                { 1.852, 0.5144, 1.1508, 1}};

        private double[,] cas = new double[5, 5] {
                { 1, 0.001, 0.00000167, 0.0000000278, 0.00000000116 },
                { 1000, 1, 0.0167, 0.0000278, 0.00000116 },
                { 60000, 60, 1, 0.0167, 0.0000694 },
                { 3600000, 3600, 60, 1, 0.0416667 },
                { 86400000, 86400, 1440, 24, 1 }};

        //pri temperaturi ni možno množenje z vnaprej določenimi koeficienti, zato tabele ni - računa se s formulami

        private double[,] povrsina = new double[8, 8] {
            { 1, 0.01, 0.0001, 0.000001, 0.000000000001, 0.0000000001, 0.00155, 0.00000000000003861 },
            { 100, 1, 0.01, 0.0001, 0.0000000001, 0.00000001, 0.155, 0.000000000003861 },
            { 10000, 100, 1, 0.01, 0.000000001, 0.0000001, 15.5, 0.0000000003861 },
            { 1000000, 10000, 100, 1, 0.000001, 0.0001, 1550.0031, 0.00000003861 },
            { 1000000000000, 10000000000, 100000000, 1000000, 1, 100, 1550003100.0062, 0.3861 },
            { 10000000000, 100000000, 1000000, 10000, 0.01, 1, 15500031.000062, 0.003861 },
            { 645.16, 6.4516, 0.064516, 0.00064516, 0.00000000064516, 0.000000064516, 1, 0.00000000002491 },
            { 2589988110336, 25899881103.36, 258998811.0336, 2589988.110336, 2.589988110336, 258.9988110336, 4014489600, 1 }};

        private double[,] prostornina = new double[8, 8] {
            { 1, 0.001, 0.000001, 0.000000001, 0.001, 0.00001, 0.000001, 0.00000610237441 },
            { 1000, 1, 0.001, 0.000001, 1, 0.01, 0.001, 0.0610237441 },
            { 1000000, 1000, 1, 0.001, 1000, 10, 1, 61.0237441 },
            { 1000000000, 1000000, 1000, 1, 1000000, 10000, 1000, 61023.7441 },
            { 1000, 1, 0.001, 0.000001, 1, 0.01, 0.001, 0.0610237441 },
            { 100000, 100, 0.1, 0.0001, 100, 1, 0.1, 6.10237441 },
            { 1000000, 1000, 1, 0.001, 1000, 10, 1, 61.0237441 },
            { 16387.064, 16.387064, 0.016387064, 0.000016387064, 16.387064, 0.16387064, 0.016387064, 1 }};

        public double pretvori(int radioIzbira, int idxLevo, int idxDesno, double stevilo)
        {
            if (radioIzbira == 0) //izbrano je pretvarjanje dolžine
                koeficient = dolzina[idxLevo, idxDesno];
            else if (radioIzbira == 1) // izbrano je pretvarjanje mase
                koeficient = masa[idxLevo, idxDesno];
            else if (radioIzbira == 2) //izbrano je pretvarjanje hitrosti
                koeficient = hitrost[idxLevo, idxDesno];
            else if (radioIzbira == 3) //izbrano je pretvarjanje časa
                koeficient = cas[idxLevo, idxDesno];
            else if (radioIzbira == 4) //izbrano je pretvarjanje temperature
                return pretvoriTemp(idxLevo, idxDesno, stevilo);
            else if (radioIzbira == 5) //izbrano je pretvarjanje površine
                koeficient = povrsina[idxLevo, idxDesno];
            else if (radioIzbira == 6) //izbrano je pretvarjanje prostornine
                koeficient = prostornina[idxLevo, idxDesno];

            return zmnozi(stevilo, koeficient); //vrne pretvorjeno št., razen v primeru temperature
        }

        private double zmnozi(double stevilo, double koeficient)
        {
            return stevilo * koeficient; //zmnoži vnseno št. s koeficientom za pretvorbo
        }

        private double pretvoriTemp(int idxLevo, int idxDesno, double stevilo)
        {
            if (idxLevo == 0) //pretvarjamo iz stopinj Celzija ...
            {
                if (idxDesno == 0) //... v stopinje Celzija
                    return stevilo;
                else if (idxDesno == 1) //... v Kelvine
                    return (stevilo + 273.15);
                else //... v stopinje Fahrenheita
                    return (stevilo * 1.8 + 32);
            }
            else if (idxLevo == 1) //pretvarjamo iz Kelvinov ...
            {
                if (idxDesno == 0) //... v stopinje Celzija
                    return (stevilo - 273.15);
                else if (idxDesno == 1) //... v Kelvine
                    return stevilo;
                else //... v stopinje Fahrenheita
                    return ((stevilo - 273.15) * 1.8 + 32);
            }
            else //pretvarjamo iz stopinj Fahrenheita ...
            {
                if (idxDesno == 0) //... v stopinje Celzija
                    return ((stevilo - 32) / 1.8);
                else if (idxDesno == 1) //... v Kelvine
                    return (((stevilo - 32) * 5) / 9 + 273.15);
                else //... v stopinje Fahrenheita
                    return stevilo;
            }
        }
    }
}
